

### Install all JS modules

- Install dependencies by running `yarn` or `npm install`.
- Run `yarn start` or `npm run start` to start the local dev server.

### Create .env files

- Creae two .env files for the Express Server and React server. 

- Requirements in the .env file for React
REACT_APP_BASE_URL=
REACT_APP_EXPRESS_URL=

- Requirements in the .env file for Express
MONGO_IP=
MONGO_PORT=
MONGO_DB=
MONGO_USER=
MONGO_PWD=

