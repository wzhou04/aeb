import React, { Fragment } from "react";
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { Hint } from 'react-autocomplete-hint';
import { AutoComplete } from "@react-md/autocomplete";
import { ReactSearchAutocomplete } from "react-search-autocomplete";
import { Loader } from '../../vibe/';
import {Component} from 'react';
import PubSub from 'pubsub-js';


// reactstrap components
import {
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    Table,
    Row,
    Col,
    InputGroup,
    InputGroupText,
    InputGroupAddon,
    Input,
    Form,
  } from "reactstrap";

class Disease extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            trait_name: '',
            snps: [],
            hintData: [],
            showTable: false,
            snpResults: true,
            options: [],
            isLoading: false,
            selectDisease: [],
            caseSensitive: false,
            ignoreDiacritics: true,
            selectRowNo: null
        };
        this.columns = [
            // { dataField: "RSID", text: "SNP ID" },
            // { dataField: "Risk_allele", text: "Risk allele" },
            // { dataField: "Risk_allele_AF", text: "Risk allele AF" },
            // { dataField: "Pubmed", text: "Pubmed", formatter: this.hyperLink },
            // { dataField: "Reported_gene", text: "Reported gene" },
            // { dataField: "P-value", text: "P-value", sort: true },
            // { dataField: "STUDY_ACCESSION", text: "Study ID"},
            { dataField: "name", text: "SNP ID", formatter: this.hyperLink},
            // { dataField: "name", text: "SNP ID"},
        ];
        this.options = {
            paginationSize: 4,
            pageStartIndex: 0,
            firstPageText: 'First',
            prePageText: 'Back',
            nextPageText: 'Next',
            lastPageText: 'Last',
            nextPageTitle: 'First page',
            prePageTitle: 'Pre page',
            firstPageTitle: 'Next page',
            lastPageTitle: 'Last page',
            showTotal: true,
            paginationTotalRenderer: this.customTotal,
            disablePageTitle: true,
            
          };

          this.selectRow = {
            mode: 'radio',
            clickToSelect: true,
            onSelect: this.handleOnSelect2,
        };
   
    }

    getTrait = async () => {
        try{
            const res = await axios.get(process.env.REACT_APP_EXPRESS_URL + '/disease/')
            //console.log(res.data);
            var hintArray = []
            res.data.map(a => hintArray.push(a.trait))
            //console.log(hintArray)
            this.setState({hintData: hintArray})
            
            const searchResults = res.data.map((i,no) => ({
                id: no,
                name: i.trait,
            }));
            // console.log(searchResults)
            this.setState({
                options: searchResults,
                isLoading: false
            })
            
            
        } catch (e) {
            console.log(e)
        }
    }
    logValue = value => {
        console.log(value);
    };


    getSNPData = async (term) => {
        try{
          //alert(trait_name)
          this.setState({isLoading: true})
          //console.log(this.state.trait_name);
          var trait_url = process.env.REACT_APP_EXPRESS_URL + "/disease/" + term.replaceAll(' ', '%20');
          console.log(trait_url);
          const data = await axios.get(trait_url);
           console.log(data);
           console.log(typeof(data));
           console.log(data.data);
           console.log(typeof(data.data));
           console.log(data.data[0].SNPs);

           const snpMap = data.data[0].SNPs.map((i,no) => ({
            id: no,
            name: i,
        }));
        console.log(snpMap)
           
           if(!data.data.length) {
               this.setState({snpResults: false})
           }else {
                this.setState({
                    // snps: data.data,
                    snps:snpMap,
                    showTable: true,
                    snpResults: true,
                    isLoading: false
                })
                
           }
           
        } catch (e) {
            console.log(e)
        }
    };

    handleChange = (event) => {
        this.setState({ trait_name : event.target.value });
    }

   
    handleOnSearch = (string, results) => {
        console.log(string, results);
    };
    
    handleOnSelect = (item) => {
        console.log("handleOnSelect")
        console.log(item.name);
        this.setState({ trait_name : item.name });
       this.getSNPData(item.name);
    };
    


    fun=(var1)=>{
        PubSub.publish("bpao", var1);
    }
    handleOnSelect2 = (row, isSelect, rowIndex, e) => {
        if (isSelect) {
            console.log("rowIndex:" + rowIndex)
            this.setState({selectRowNo: rowIndex})
            
            const redirect_1=this.fun(this.state.snps[rowIndex].name);
            console.log(this.state.snps[rowIndex].name);
        
            // const res_generateTable = this.generateTableData(rowIndex);
            // console.log("Finish generateTable:")
            // console.log(res_generateTable);
            
        }
      } 
   

    handleOnFocus = () => {
        console.log("Focused");
        console.log("trait_name:")
        console.log(this.state.trait_name)
        
    };

    // hyperLink = (cell, row) => {
    //     return (
    //         <div><a href={"https://pubmed.ncbi.nlm.nih.gov/"+cell} target="_blank">{cell}</a></div>
    //     );
    // }

    hyperLink = (cell, row) => {
        return (
            <div><a href={"http://10.238.31.237:3000/snp"} target="_blank">{cell}</a></div>
        );
    }
    
    customTotal = (from, to, size) => (
      <span className="react-bootstrap-table-pagination-total">
        Showing { from } to { to } of { size } Results
      </span>
    );
  
    componentDidMount() {
        this.getTrait();
    }

    render() {
        return (
            <div className="content">
                <Row>
                    
                    <Col md="12">
                        <Card>
                            <CardHeader>
                            <CardTitle tag="h4">Disease</CardTitle>
                            </CardHeader>
                            <CardBody>
                            <ReactSearchAutocomplete
                                items={this.state.options}
                                maxResults={15}
                                onSearch={this.handleOnSearch}
                                onSelect={this.handleOnSelect}
                                onFocus={this.handleOnFocus}
                                styling={{ zIndex: 2 }} // To display it on top of the search box below
                                autoFocus
                            />
                                
                                
                            </CardBody>
                        </Card>
                    </Col>
                    { (this.state.showTable && this.state.snpResults && !this.state.isLoading)
                        ?    <Col md="12">
                                <Card>
                                    <CardHeader>
                                    <CardTitle tag="h4">{this.state.trait_name} relevant SNPs</CardTitle>
                                    </CardHeader>
                                    <CardBody>
                                    <div className="SNPTable">
                                        <BootstrapTable
                                            keyField="id"
                                            data={this.state.snps}
                                            columns={this.columns}
                                            selectRow={this.selectRow }
                                            striped
                                            hover
                                            condensed
                                            pagination={paginationFactory(this.options)}
                                        />       
                                    </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        : (this.state.snpResults == false && !this.state.isLoading) ? 
                                <Col md="12">
                                <Card>
                                    <CardHeader>
                                    <CardTitle tag="h4">Can't find any {this.state.trait_name} relevant SNPs</CardTitle>
                                    </CardHeader>
                                </Card>
                            </Col>
                        : (this.state.isLoading) ? 
                            <Col md="12">
                                <Loader type="spin" />
                            </Col>
                        : null 
                    }
                </Row>
            </div>
        );
    }
}

export default Disease;