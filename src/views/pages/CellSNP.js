import React from "react";
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { HorizontalBar } from 'react-chartjs-2';
import { MDBContainer } from 'mdbreact';
import igv from 'igv/dist/igv';
import { Loader } from '../../vibe/';
import ToolkitProvider, { CSVExport } from 'react-bootstrap-table2-toolkit';
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardTitle,
    FormGroup,
    Form,
    Input,
    Row,
    Col,
    Label,
} from "reactstrap";
import { TrendingUp } from "react-feather";

var igvStyle = {
    paddingTop: '10px',
    paddingBottom: '10px',
    margin: '8px',
    border: '1px solid lightgray'
}

var igvStyle = {
paddingTop: '10px',
paddingBottom: '10px',
margin: '8px',
border: '1px solid lightgray'
}



class CellSNP extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            rsid: '',
            cell: 'NHCFV',
            snps: [],
            selectedSNP: [],
            promoters: [],
            chr: '',
            pos: '',
            ref: '',
            alt: '',
            rsid_url: '',
            locus_range: '',
            locus_hic_url: '',
            locus_snp_url: '',
            atac_url: '',
            // dnase_url: '',
            chromHMM_url: '',
            chromHMM_url_E104: '',
            chromHMM_url_E105: '',
            H3k27ac_url_lv: '',
            H3k27ac_url_lv: '',
            H3k4me3_url_lv: '',
            H3k4me3_url_rv: '',
            H3K4me1_url_lv: '',
            H3K4me1_url_rv: '',
            CTCF_url: '',
            H3k9me3_url: '',
            H3k27me3_url: '',
            H3k36me3_url_lv: '',
            H3k36me3_url_rv: '',
            H3k9me3_url_lv: '',
            H3k9me3_url_rv: '',
            RAD21_url: '',
            SMC3_url: '',
            // H3k4me1_url: '',
            // H3k9ac_url: '',
            hic: '',
            selectRowNo: null,
            showTable: false,
            snpResults: false,
            promoterResults: false,
            showIGV: false,
            multiSNP: false,
            isLoading: false,
            dataHorizontal1000G: {
                labels: ['ALL','AFR','AMR','EAS','EUR','SAS'],
                datasets: [
                    {
                        label: '1000g',
                        data: [],
                        fill: false,
                        backgroundColor: [
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)'
                        ],
                        borderColor: [
                            'rgba(54, 162, 235)',
                            'rgba(54, 162, 235)',
                            'rgba(54, 162, 235)',
                            'rgba(54, 162, 235)',
                            'rgba(54, 162, 235)',
                            'rgba(54, 162, 235)'
                        ],
                        borderWidth: 1
                    }
                ]
            },
            dataHorizontalgnomad: {
                labels: ['ALL','AFR','AMR','EAS','NFE','SAS'],
                datasets: [
                    {
                        label: 'gnomAD',
                        data: [],
                        fill: false,
                        backgroundColor: [
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)'
                        ],
                        borderColor: [
                            'rgba(54, 162, 235)',
                            'rgba(54, 162, 235)',
                            'rgba(54, 162, 235)',
                            'rgba(54, 162, 235)',
                            'rgba(54, 162, 235)',
                            'rgba(54, 162, 235)'
                        ],
                        borderWidth: 1
                    }
                ]
            },
            SNPColumns: [],
        };
        
        this.PromoterColumns = [
            { dataField: "Chr", text: "Promoter"},
            { dataField: "Gene", text: "Gene" },
            { dataField: "TSS", text: "TSS" },
            { dataField: "Transcript", text: "Transcript"},
            { dataField: "HiC_Promoter_bin", text: "Hi-C Promter bin" },
            { dataField: "HiC_info", text: "Hi-C info" },
            { dataField: "ChromHMM", text: "ChromHMM"},
            { dataField: "RefTSS", text: "RefTSS"},
            { dataField: "ENCODE-cCRE-PLS", text: "ENCODE-PLS"},
        ];
        this.MultiSNPColumns = [
            { dataField: "#Chr", text: "Chr"},
            { dataField: "Start", text: "Position"},
            { dataField: "Ref", text: "Ref"},
            { dataField: "Alt", text: "Alt"},
            { dataField: "AF", text: "AF"},
        ];
        this.options = {
            paginationSize: 4,
            pageStartIndex: 0,
            firstPageText: 'First',
            prePageText: 'Back',
            nextPageText: 'Next',
            lastPageText: 'Last',
            nextPageTitle: 'First page',
            prePageTitle: 'Pre page',
            firstPageTitle: 'Next page',
            lastPageTitle: 'Last page',
            showTotal: true,
            paginationTotalRenderer: this.customTotal,
            disablePageTitle: true,
            
          };

        this.selectRow = {
            mode: 'radio',
            clickToSelect: true,
            onSelect: this.handleOnSelect,
        };
        
    }



    handleOnSelect = (row, isSelect, rowIndex, e) => {
        if (isSelect) {
            console.log("rowIndex:" + rowIndex)
            this.setState({selectRowNo: rowIndex})
            //console.log(this.selectRowNo)
            //this.state.selectRowNo = 0 + rowIndex
            
            const res_generateTable = this.generateTableData(rowIndex);
            console.log("Finish generateTable:")
            console.log(res_generateTable);
            
        }
    }  
      
      
    componentDidMount() {
        console.log("componentdidMount start")
        this.setState({
            cell: this.props.match.params.cell,
        })
        this.getSNPData();
        console.log("componentdidMount end")
        //console.log("componentDidMount locus_range" + this.state.locus_range)
    }


  componentDidUpdate(prevProps, prevState) {
    console.log("PROCESS!!!--componentDidUpdate");
    if(prevState.showIGV !== this.state.showIGV){
        console.log('showIGV state has changed:' + this.state.showIGV);
        //console.log('range state has changed:' + this.state.locus_range)
        //console.log('prevState.locus_range:' + prevState.locus_range)
        
        var igvContainer = document.getElementById('igv-div');
        var igvOptions;
        if(this.state.promoterResults){
            console.log("part if ");
            igvOptions = {
                genome: 'hg38', 
                
                locus: this.state.locus_range,
                thickness: 2,
                tracks: [
                    {
                        url: this.state.locus_hic_url,
                        type: "interaction",
                        format: "bedpe",
                        name: "Significant Hi-C",
                        useScore: true,
                        arcType: "nested",
                        color: "blue",
                        logScale: true,
                        showBlocks: true,
                        height: 150
                    },
                  
                    {
                        type: "annotation",
                        format: "bed",
                        url: this.state.locus_snp_url,
                        indexURL: false,
                        name: this.state.rsid,
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.atac_url,
                        height: 50,
                        name: "ATAC-seq",
                    },
                    // {
                    //     type: "wig",
                    //     format: "bigwig",
                    //     url: this.state.dnase_url,
                    //     height: 50,
                    //     name: "Dnase-seq",
                    // },
                    {
                        type: "annotation",
                        format: "bed",
                        url: this.state.chromHMM_url_E104,
                        height: 50,
                        name: "ChromHMM_E104",
                        displayMode: "EXPANDED",
                    },
                    // {
                    //     type: "annotation",
                    //     format: "bed",
                    //     url: this.state.chromHMM_url_3,
                    //     height: 50,
                    //     name: "ChromHMM_E095",
                    //     displayMode: "EXPANDED",
                    // },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me3_url_lv,
                        height: 50,
                        name: "H3k4me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k36me3_url_lv,
                        height: 50,
                        name: "H3k36me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me1_url_lv,
                        height: 50,
                        name: "H3k4me1 Left Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k9me3_url_lv,
                        height: 50,
                        name: "H3k9me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27me3_url_lv,
                        height: 50,
                        name: "H3k27me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27ac_url_lv,
                        height: 50,
                        name: "H3k27ac Left Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    
                   
                   

                    // {
                    //     type: "wig",
                    //     format: "bigwig",
                    //     url: this.state.CTCF_url,
                    //     height: 50,
                    //     name: "CTCF",
                    //     color: "rgb(252, 74, 3)",
                    // },

                   

                 
                    {
                        type: "annotation",
                        format: "bed",
                        url: this.state.chromHMM_url_E105,
                        height: 50,
                        name: "ChromHMM_E105",
                        displayMode: "EXPANDED",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me3_url_rv,
                        height: 50,
                        name: "H3k4me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k36me3_url_rv,
                        height: 50,
                        name: "H3k36me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me1_url_rv,
                        height: 50,
                        name: "H3k4me1 Right Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k9me3_url_rv,
                        height: 50,
                        name: "H3k9me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27me3_url_rv,
                        height: 50,
                        name: "H3k27me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27ac_url_rv,
                        height: 50,
                        name: "H3k27ac Right Ventricle",
                        color: "rgb(252, 202, 3)",
                    },


                    {
                        type: "annotation",
                        format: "bb",
                        url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/encodeCcreCombined.bb',
                        height: 50,
                        name: "ENCODE-cCRE",
                        displayMode: "EXPANDED",
                    },
                    {
                        type: "annotation",
                        format: "bed",
                        url: process.env.REACT_APP_BASE_URL + "/igv/promoter_like_regions_annotation_sorted.bed",
                        height: 50,
                        name: "Promoter-like-region",
                        displayMode: "EXPANDED",
                    },
                   
                    {
                        type: "annotation",
                        format: "gtf",
                        url: process.env.REACT_APP_BASE_URL + '/igv/gencode.v35.annotation.sort.gtf.gz',
                        indexURL: process.env.REACT_APP_BASE_URL + '/igv/gencode.v35.annotation.sort.gtf.gz.tbi',
                        displayMode: "EXPANDED",
                        name: "Gencode v35 (gtf)",
                        visibilityWindow: 10000000
                    },
                ]
            };
        }else{
            console.log("part else");
            igvOptions = {
                genome: 'hg38', 
                locus: this.state.locus_range,
                thickness: 2,
                tracks: [
                    {
                        type: "annotation",
                        format: "bed",
                        url: this.state.locus_snp_url,
                        indexURL: false,
                        name: this.state.rsid,
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.atac_url,
                        height: 50,
                        name: "ATAC-seq",
                    },
                    // {
                    //     type: "wig",
                    //     format: "bigwig",
                    //     url: this.state.dnase_url,
                    //     height: 50,
                    //     name: "Dnase-seq",
                    // },
                    {
                        type: "annotation",
                        format: "bed",
                        url: this.state.chromHMM_url_E104,
                        height: 50,
                        name: "ChromHMM_E104",
                        displayMode: "EXPANDED",
                    },
                    // {
                    //     type: "annotation",
                    //     format: "bed",
                    //     url: this.state.chromHMM_url_3,
                    //     height: 50,
                    //     name: "ChromHMM_E095",
                    //     displayMode: "EXPANDED",
                    // },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me3_url_lv,
                        height: 50,
                        name: "H3k4me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k36me3_url_lv,
                        height: 50,
                        name: "H3k36me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me1_url_lv,
                        height: 50,
                        name: "H3k4me1 Left Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k9me3_url_lv,
                        height: 50,
                        name: "H3k9me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27me3_url_lv,
                        height: 50,
                        name: "H3k27me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27ac_url_lv,
                        height: 50,
                        name: "H3k27ac Left Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    
                   
                   

                    // {
                    //     type: "wig",
                    //     format: "bigwig",
                    //     url: this.state.CTCF_url,
                    //     height: 50,
                    //     name: "CTCF",
                    //     color: "rgb(252, 74, 3)",
                    // },

                   

                 
                    {
                        type: "annotation",
                        format: "bed",
                        url: this.state.chromHMM_url_E105,
                        height: 50,
                        name: "ChromHMM_E105",
                        displayMode: "EXPANDED",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me3_url_rv,
                        height: 50,
                        name: "H3k4me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k36me3_url_rv,
                        height: 50,
                        name: "H3k36me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me1_url_rv,
                        height: 50,
                        name: "H3k4me1 Right Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k9me3_url_rv,
                        height: 50,
                        name: "H3k9me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27me3_url_rv,
                        height: 50,
                        name: "H3k27me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27ac_url_rv,
                        height: 50,
                        name: "H3k27ac Right Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    {
                        type: "annotation",
                        format: "bed",
                        url: process.env.REACT_APP_BASE_URL + "/igv/promoter_like_regions_annotation_sorted.bed",
                        height: 50,
                        name: "Promoter-like-region",
                        displayMode: "EXPANDED",
                    },
                  
                    {
                        type: "annotation",
                        format: "gtf",
                        url: process.env.REACT_APP_BASE_URL + '/igv/gencode.v35.annotation.sort.gtf.gz',
                        indexURL: process.env.REACT_APP_BASE_URL + '/igv/gencode.v35.annotation.sort.gtf.gz.tbi',
                        displayMode: "EXPANDED",
                        name: "Gencode v35 (gtf)",
                        visibilityWindow: 10000000
                    },
                ]
            };
        }
        
        return igv.createBrowser(igvContainer, igvOptions);

    }
    console.log("PROCESS!!!END!!!--componentDidUpdate");
}





  handleChangeSNP =(event) =>{
    console.log("PROCESS!!!--handleChangeSNP");
    this.setState({rsid: event.target.value});
    console.log("PROCESS!!!END!!!--handleChangeSNP");


  }

//   handleSubmit =(event) =>{
//     console.log("PROCESS!!!--handleSumbit");
//     console.log("this is handleSubmit")
//     this.getSNPData();
//     event.preventDefault();
//     console.log("PROCESS!!!END!!!--handleSumbit");
//   }

getSNPData = async () => {
    try{
      //alert(trait_name)
      //console.log(this.state.trait_name);
      //var snp_url = "http://localhost:5000/snp/" + this.state.rsid;
      var snp_url = process.env.REACT_APP_EXPRESS_URL + "/variantID/" + this.props.match.params.variantID;
      console.log(snp_url);
      const res = await axios.get(snp_url);
      console.log(res.data);
      if(!res.data.length) {
        this.setState({
            snpResults: false,
            showTable: true,
        })
      }else if (res.data.length > 1){ // multiple SNPs within this rsID
        console.log("Multiple SNP:" + res.data.length);
        console.log("else if condition--multiple snp");
        this.setState({
            snps: res.data,
            multiSNP: true,
        })
        
        
      }else { //only on SNP within this rsID
        //var index = 0
        console.log("final else");
        this.setState({
            snps: res.data,
        })
        const res_generateTable = await this.generateTableData(0);
        console.log("res_generateTable:")
        console.log(res_generateTable);
        
        
      }
       
      console.log("END TRY")
       
    } catch (e) {
        console.log(e)
    }
};

  generateTableData = async (index) => {
    console.log("PROCESS!!!--generateTableData");
    var annotation = this.state.snps[index];
    var anno_json = [...this.state.snps];
    console.log('APP_URL:')
    
    //console.log("origin anno_json:" + JSON.stringify(anno_json))
    var arr = [];
    for(var i=0; i< this.state.snps.length; i++){
        if(i == index){
            arr.push(anno_json[i])
        }
    }
    //console.log("selectRowNo:" + this.state.selectRowNo)
    console.log(arr)
    //var arr = JSON.parse(JSON.stringify(anno_json));
    //console.log("arr:" + arr)
    this.setState({ selectedSNP: arr})

    console.log("Alt:" + annotation.Alt)
    var dataHorizontal1000G_update = {...this.state.dataHorizontal1000G};
    var dataHorizontalgnomad_update = {...this.state.dataHorizontalgnomad};
    var AF_1000G = [annotation["1000g_ALL"],annotation["1000g_AFR"],annotation["1000g_AMR"],annotation["1000g_EAS"],annotation["1000g_EUR"],annotation["1000g_SAS"]];
    var AF_gnomad = [annotation.gnomAD_All,annotation.gnomAD_AFR,annotation.gnomAD_AMR,annotation.gnomAD_EAS,annotation.gnomAD_NFE,annotation.gnomAD_SAS];
    dataHorizontal1000G_update.datasets[0].data = AF_1000G;
    dataHorizontalgnomad_update.datasets[0].data = AF_gnomad;
    dataHorizontalgnomad_update.datasets[0].label = "gnomAD";
    var rsidURL = "https://www.ncbi.nlm.nih.gov/snp/" + this.state.rsid;
    
    this.setState({
        chr: annotation["#Chr"],
        pos: annotation.Start,
        ref: annotation.Ref,
        alt: annotation.Alt,
        rsid_url: "https://www.ncbi.nlm.nih.gov/snp/" + this.state.rsid,
        
        dataHorizontal1000G: dataHorizontal1000G_update,
        dataHorizontalgnomad: dataHorizontalgnomad_update,
    })
        
    const response = await fetch(process.env.REACT_APP_EXPRESS_URL + '/writeFile',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            type: "snp",
            post: this.state.rsid,
            range: "chr" + annotation["#Chr"] + "\t" + annotation.Start + "\t" + annotation.Start + "\t" + this.state.rsid,
        }),
    });
    const writeRes = await response.text();
    console.log("write SNP file:")
    console.log(writeRes)
    

    if (this.state.cell === "NHCFV") {
        console.log("NHCFV cell-type processing...")
        var columns_update = [
            { dataField: "Region", text: "Region" },
            { dataField: "GeneName_ID_Ensembl", text: "Gene" },
            { dataField: "GeneInfo_DistNG_Ensembl", text: "Distance with Gene" },
            { dataField: "Promoter_like_region", text: "Promoter-Like" },
            // { dataField: "chromHMM_E026_25", text: "ChromHMM" },
            // { dataField: "OpenChromatin_hMSC", text: "Open Chromatin"},
            { dataField: "SigHiC_NHCFV", text: "Sig Hi-C"},
        ]
        this.setState({
            SNPColumns: columns_update,
            atac_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/NHCFVcombined_R1.trim.merged.nodup.tn5.pval.signal.bigwig',
            // chromHMM_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/E129_25_imputed12marks_hg38lift_dense.bb',
            chromHMM_url_E104: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/E104_25_imputed12marks_hg38lift_dense.bed',
            chromHMM_url_E105: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/E095_25_imputed12marks_hg38lift_dense.bed',

            H3k4me3_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF285MYB.bigWig',
            H3k36me3_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF815KRX.bigWig',
            H3k4me1_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF213UYA.bigWig',
            H3k9me3_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF294DMS.bigWig',
            H3k27me3_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF289STQ.bigWig',
            H3k27ac_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF094PUR.bigWig',
            H3k4me3_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF026UGX.bigWig',
            H3k36me3_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF443LCS.bigWig',
            H3k4me1_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF240MYO.bigWig',
            H3k9me3_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF136HCH.bigWig',
            H3k27me3_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF217IRO.bigWig',
            H3k27ac_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF446QTP.bigWig',
            // dnase_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/DNase_seq_hMSC.bigWig',
            // chromHMM_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/E026_25_imputed12marks_hg38lift_dense.bed',
            // H3k27ac_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/NHCFV_H3K27ac_r1.pvalue_signal.bw',
            // H3k4me3_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/NHCFV_H3K4me3_r1.pvalue_signal.bw',
            // CTCF_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/NHCFV_CTCF_r1.pvalue_signal.bw',
            // H3k9me3_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/NHCFV_H3K9me3_r1.pvalue_signal.bw',
            // H3k27me3_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/NHCFV_H3K27me3_r1.pvalue_signal.bw',
            // H3k36me3_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/NHCFV_H3K36me3_r1.pvalue_signal.bw',
            // RAD21_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/NHCFV_RAD21_r1.pvalue_signal.bw',
            // SMC3_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/NHCFV_SMC3_r1.pvalue_signal.bw',
            
            // H3k4me1_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/ENCFF648BRC_hMSC_H3K4me1_pvalue.bigWig',
            // H3k9ac_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/ENCFF757SDY_hMSC_H3K9ac_pvalue.bigWig',
        })
        console.log(this.state.atac_url)
        console.log("sigHiC:" + annotation.SigHiC_NHCFV)
        
        if(annotation.SigHiC_NHCFV){
            this.setState({hic: annotation.SigHiC_NHCFV});
            
            const hicProcess_res = await this.hicProcess();
            console.log("hicProcess_res:")
            console.log(hicProcess_res);
        }else{
            this.setState({ promoterResults: false})
        }
        
    }
    if(this.state.promoterResults == false){
        var start_pos = parseInt(this.state.pos) - 10000
        var end_pos = parseInt(this.state.pos) + 10000
        if (start_pos < 0){
            start_pos = 0
        }
        this.setState({ locus_range: "chr" + this.state.chr + ":" + start_pos + "-" + end_pos})
        console.log("locus_range:" + this.state.locus_range)
    }
    this.setState({ 
        showTable: true,
        snpResults: true,
        showIGV: true,
        isLoading: false
    })
    console.log("PROCESS!!!END!!!--generateTableData");
    return "Finish generateTable";
    
};



hicProcess = async() => {
    console.log("PROCESS!!!--hicProcess");
    //console.log(this.state.hic);
    let reg_bin_Re = RegExp('RegulatoryBin:(.*?);')
    let reg = reg_bin_Re.exec(this.state.hic)[1]
    console.log("reg:" + reg)
    const reg_range = reg.split(':')
    var reg_content = "chr" + this.state.chr + "\t" + reg_range[1] + "\t" + reg_range[2];
    var igv_bedpe_content;
    var start_min = parseInt(reg_range[1])
    var end_max = parseInt(reg_range[2])
    console.log('start_min:' + start_min + "; end_max:" + end_max)
    var promoter_api_url;
    if(this.state.cell === "NHCFV"){
        // promoter_api_url = process.env.REACT_APP_EXPRESS_URL + '/promoter/' + reg.replaceAll(':', '%3A');
        promoter_api_url = "http://10.238.31.237:8080/promoter/" + reg.replaceAll(':', '%3A');
    }
    console.log(promoter_api_url);
    try{
        const res_promoter = await axios.get(promoter_api_url);
        console.log(res_promoter.data)
        var promoter_update = res_promoter.data
        for(var i=0;i<promoter_update.length;i++){
            const hicPromoterBin = promoter_update[i].HiC_Promoter_bin.split(':')
            const hicPromoterBinChr = hicPromoterBin[0];
            const hicPromoterBinStart = hicPromoterBin[1];
            const hicPromoterBinEnd = hicPromoterBin[2];
            console.log(hicPromoterBinChr)
            console.log(hicPromoterBinStart)
            console.log(hicPromoterBinEnd)
            if(promoter_update[i].Start < start_min){
            
                start_min = hicPromoterBinStart
            }
            if(hicPromoterBinEnd > end_max){
                end_max = hicPromoterBinEnd
            }
            // var each_interaction = reg_content + "\t" + promoter_update[i].Chr + "\t" + promoter_update[i].Start + "\t" + promoter_update[i].End + "\t10\n";
            var each_interaction = reg_content + "\t" + "chr"+promoter_update[i].Chr + "\t" + promoter_update[i].Start + "\t" + promoter_update[i].End + "\t10\n";

            if(igv_bedpe_content == null){
                igv_bedpe_content = each_interaction
            }else{
                igv_bedpe_content += each_interaction
            }
            
            
            promoter_update[i].Chr = promoter_update[i].Chr + ":" + promoter_update[i].Start + "-" + promoter_update[i].End
            
        }
        //console.log('start_min:' + start_min + "; end_max:" + end_max)
        //console.log(igv_bedpe_content)
        this.setState({
            locus_hic_url: process.env.REACT_APP_BASE_URL + "/igv/" + this.state.rsid + "_" + this.state.cell + ".bedpe.txt",
            promoters: res_promoter.data,
            promoterResults: true
        })
        //const response = await fetch('http://localhost:5000/writeFile',{
        const response = await fetch(process.env.REACT_APP_EXPRESS_URL + '/writeFile',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                type: "interaction",
                cell: this.state.cell,
                rsid: this.state.rsid,
                content: igv_bedpe_content,
            }),
        });
        const writeRes = await response.text();
        console.log("write file:")
        console.log(writeRes)

        var final_render_sigHiC = this.state.hic.replaceAll(';', '\n');
        final_render_sigHiC = final_render_sigHiC.replaceAll('RegulatoryBin:', 'RegulatoryBin\n')
        final_render_sigHiC = final_render_sigHiC.replaceAll('RegulatoryBin:', 'RegulatoryBin\n')
        final_render_sigHiC = final_render_sigHiC.replaceAll(',', '\n')
        console.log("clean hic:" + final_render_sigHiC)
        var start_pos = parseInt(start_min) - 10000
        var end_pos = parseInt(end_max) + 10000
        if (start_pos < 0){
            start_pos = 0
        }
        this.setState({ locus_range: "chr" + this.state.chr + ":" + start_pos + "-" + end_pos})
        console.log("locus_range:" + this.state.locus_range)
    } catch (error) {
        console.log(error);
    }
    console.log("PROCESS!!!END--hicProcess");
    return "hicProcess finished";
    
};




  render(){
    return(
    <div>
      <Row>
        {/* <Col md="12">
          <Card>
            <CardHeader>
              <CardTitle tag="h4">SNP</CardTitle>
            </CardHeader>
            <CardBody>
            <Form onSubmit={this.handleSubmit}>
              <FormGroup>
                <Input type='text'
                value={this.state.rsid} 
                placeholder="Search SNP"
                onChange={this.handleChangeSNP}/>
              </FormGroup>
            <FormGroup>
              <Label>Select Cell Type</Label>
              <Input type="select">
                <option>NHCFV</option>
                <option>NHCFA</option>
                <option>Cardiomyocytes</option>
              </Input>
            </FormGroup>
            <Button>Submit</Button>
            </Form>
            </CardBody>
          </Card>
        </Col> */}

        {(this.state.multiSNP === true)
                        ? <Col md="12">
                            <Card>
                                <CardHeader>
                                <CardTitle tag="h4">{this.props.match.params.variantID} has multiple SNPs</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <div className="content">
                                        <BootstrapTable
                                            keyField="_id"
                                            data={this.state.snps}
                                            // data={this.props.match.params.variantID}
                                            columns={this.MultiSNPColumns}
                                            //selectRow={this.onSelect}
                                            selectRow={this.selectRow }
                                            striped
                                            hover
                                            condensed
                                        />       
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                        : null
                    }
                    
                    { (this.state.snpResults)
                        ? <Col md="2">
                            <Card className="content">
                                
                                <CardBody>
                                    <h5>
                                    Chr: {this.state.chr} 
                                    </h5>
                                    <h5>
                                    Pos: {this.state.pos} 
                                    </h5>
                                    <h5>
                                    Ref: {this.state.ref} 
                                    </h5>
                                    <h5>
                                    Alt: {this.state.alt} 
                                    </h5>
                                    <h5>
                                        rsID:
                                        <a
                                        href={this.state.rsid_url} 
                                        target="_blank"
                                        >
                                        {this.state.rsid}
                                        </a>
                                    </h5>
                                </CardBody>
                                
                            </Card>
                        </Col>
                        : null
                    }
                    { (this.state.snpResults)
                        ? <Col md="5">
                            <Card className="content">
                                <CardBody>
                                    <MDBContainer>
                                        <HorizontalBar
                                            data={this.state.dataHorizontal1000G}
                                            options={{ responsive: true }}
                                        />
                                    </MDBContainer>
                                </CardBody>
                            </Card>
                        </Col>
                        : null
                    }
                    { (this.state.snpResults)
                        ? <Col md="5">
                            <Card className="content">
                                <CardBody>
                                    <MDBContainer>
                                        <HorizontalBar
                                            data={this.state.dataHorizontalgnomad}
                                            options={{ responsive: true }}
                                        />
                                    </MDBContainer>
                                </CardBody>
                            </Card>
                        </Col>
                        : null
                    }
                    {(this.state.snpResults && this.state.showIGV)
                        ? <Col md="12">
                            <Card>
                                <CardHeader>
                                <CardTitle tag="h4">{this.state.rsid}'s IGV in {this.state.cell} cell-type</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <div id="igv-div" style={igvStyle}></div>
                                </CardBody>
                            </Card>
                        </Col>
                        : null
                    }
                    {/* { (this.state.showTable && this.state.snpResults) */}
                    { (this.state.snpResults && !this.state.isLoading)
                        ?    
                            <Col md="12">
                                <Card>
                                    <CardHeader>
                                    <CardTitle tag="h4">{this.state.rsid} in {this.state.cell} cell-type</CardTitle>
                                    </CardHeader>
                                    <CardBody>
                                    <div className="SNPTable">
                                        <BootstrapTable
                                            keyField="_id"
                                            //data={this.state.snps}
                                            data={this.state.selectedSNP}
                                            columns={this.state.SNPColumns}
                                            striped
                                            hover
                                            condensed
                                            //pagination={paginationFactory()}
                                        />       
                                    </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        : (this.state.snpResults === false && this.state.showTable === true) ? 
                                <Col md="12">
                                <Card>
                                    <CardHeader>
                                    <CardTitle tag="h4">Can't find any {this.props.match.params.variantID}  relevant SNPs</CardTitle>
                                    </CardHeader>
                                </Card>
                            </Col>
                        : (this.state.isLoading) ? 
                            <Col md="12">
                                <Loader type="spin" />
                            </Col>
                        : null 
                    }
                    {/* { (this.state.showTable && this.state.snpResults && this.state.promoterResults) */}
                    { (this.state.snpResults && this.state.promoterResults && !this.state.isLoading)
                        ? <Col md="12">
                            <Card>
                                <CardHeader>
                                <CardTitle tag="h4">{this.state.rsid}'s Hi-C interactions in {this.state.cell} cell-type</CardTitle>
                                </CardHeader>
                                <CardBody>
                                <div className="content">
                                    <BootstrapTable
                                        keyField="_id"
                                        data={this.state.promoters}
                                        columns={this.PromoterColumns}
                                        striped
                                        hover
                                        condensed
                                        pagination={paginationFactory()}
                                    />       
                                </div>
                                </CardBody>
                            </Card>
                        </Col>
                        
                        : null
                    }




      </Row>
    </div>
    );
  }
}


export default CellSNP;