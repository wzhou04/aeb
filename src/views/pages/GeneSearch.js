import React from "react";
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { Loader } from '../../vibe/';
import classnames from 'classnames';
import ToolkitProvider, { CSVExport } from 'react-bootstrap-table2-toolkit';
import igv from 'igv/dist/igv';



import {
    Button,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardTitle,
    FormGroup,
    Form,
    Input,
    Row,
    Col,
    Label,
    TabContent, TabPane, Nav, NavItem, NavLink
} from "reactstrap";

var igvStyle = {
    paddingTop: '10px',
    paddingBottom: '10px',
    margin: '8px',
    border: '1px solid lightgray'
  }

class Gene1 extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            rsid: '',
            cell: 'NHCFV',
            snps: [],
            geneData: [],
            selectedSNP: [],
            promoters: [],
            chr: '',
            pos: '',
            ref: '',
            alt: '',
            rsid_url: '',
            gene: '',
            locus_range: '',
            locus_hic_url: '',
            locus_snp_url: '',
            atac_url: '',
            dnase_url: '',
            chromHMM_url: '',
            chromHMM_url_E104: '',
            chromHMM_url_E105: '',
            H3k27ac_url_lv: '',
            H3k27ac_url_lv: '',
            H3k4me3_url_lv: '',
            H3k4me3_url_rv: '',
            H3K4me1_url_lv: '',
            H3K4me1_url_rv: '',
            CTCF_url: '',
            H3k9me3_url: '',
            H3k27me3_url: '',
            H3k36me3_url_lv: '',
            H3k36me3_url_rv: '',
            H3k9me3_url_lv: '',
            H3k9me3_url_rv: '',
            gencode_url: '',
            encode_ccre_url: '',
            hic: '',
            selectRowNo: null,
            showTable: false,
            snpResults: false,
            showPormoterTable: false,
            promoterResults: false,
            showIGV: false,
            isLoading: false,
            SNPColumns: [],
            geneInfo: false,
            geneLoading: false,
            promoterData: [],
            promoterInfo: false,
            promoterLoading: false,
            tssList: [],
            proximalRegionInfo: false,
            proximalRegionLoading:false,
            showProximalTable: false,
            proximalResults: false,
            proximalData: [],
            final_allProximalList: [],
        };
        this.exonicColumns = [
            { dataField: "RSID", text: "rsID"},
                    // { dataField: "ExonicFunc_Ensembl", text: "Function"},
                    { dataField: "AAChange_Ensembl", text: "AAChange"},
                    // { dataField: "Promoter_like_region", text: "Promoter"},
                    // { dataField: "chromHMM_E026_25", text: "ChromHMM"},
                    // { dataField: "OpenChromatin_hMSC", text: "Open Chromatin"},
                    // { dataField: "ATAC-MBPS_hMSC-Day1", text: "TF footprinting"},
                    { dataField: "SigHiC_NHCFV", text: "Hi-C info"},
        ]
       
        this.promoterColumns =[
            { dataField: "Chr", text: "Chr"},
            { dataField: "Start", text: "Start"},
            { dataField: "End", text: "End"},
            { dataField: "TSS", text: "TSS"},
            { dataField: "Gene", text: "Gene"},
            { dataField: "HiC_Promoter_bin", text: "HiC Promoter bin"},
            { dataField: "HiC_Distal_bin", text: "HiC Distal bin"},
            { dataField: "HiC_info", text: "HiC info"},
        ]

        this.proximalRegionColumns=[
            { dataField: "RSID", text: "rsID"},
            { dataField: "Start", text: "Start"},
            { dataField: "Region_Ensembl", text: "Region"},
            { dataField: "#Chr", text: "Chr"},
            { dataField: "AAChange_Ensembl", text: "AAChange"},
        ]

        this.geneColumns =[

            { dataField: "gene_symbol", text: "Gene Name"},
            { dataField: "chr", text: "Char"},
            { dataField: "start", text: "Start"},
            { dataField: "end", text: "End"},
            { dataField: "full_name", text: "Full Name"},
            { dataField: "alias", text: "Alias"},
            
        ]


        const { ExportCSVButton } = CSVExport;

        
        this.options = {
            paginationSize: 4,
            pageStartIndex: 0,
            firstPageText: 'First',
            prePageText: 'Back',
            nextPageText: 'Next',
            lastPageText: 'Last',
            nextPageTitle: 'First page',
            prePageTitle: 'Pre page',
            firstPageTitle: 'Next page',
            lastPageTitle: 'Last page',
            showTotal: true,
            paginationTotalRenderer: this.customTotal,
            disablePageTitle: true,
            
          };

          this.selectSNP = {
            mode: 'radio',
            clickToSelect: true,
            style: { backgroundColor: '#c8e6c9' },
            onSelect: this.SNPDetail,
        }
        
    }
    toggle(tab) {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab,
          });
        }
    }

    componentDidMount() {
        console.log("showIGV state" + this.state.showIGV)
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(prevState.showIGV !== this.state.showIGV){
            console.log('showIGV state has changed:' + this.state.showIGV);
            //console.log('range state has changed:' + this.state.locus_range)
            //console.log('prevState.locus_range:' + prevState.locus_range)
            
            var igvContainer = document.getElementById('igv-div');
            
            var igvOptions = {
                genome: 'hg38', 
                //locus: 'BRCA1'
                //locus: "chr8:118835887-119042590",
                locus: this.state.locus_range,
                thickness: 5,
                tracks: [
                    {
                        url: this.state.locus_hic_url,
                        type: "interaction",
                        format: "bedpe",
                        name: "Significant Hi-C",
                        //arcType: "proportional",
                        useScore: true,
                        arcType: "nested",
                        //color: "rgb(204,102,0)",
                        color: "blue",
                        //alpha: "0.05",
                        logScale: true,
                        showBlocks: true,
                        //max: 80,
                        visibilityWindow: 10000000,
                        height: 150
                    },
                    
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.atac_url,
                        height: 50,
                        name: "ATAC-seq",
                    },
                    // {
                    //     type: "wig",
                    //     format: "bigwig",
                    //     url: this.state.dnase_url,
                    //     height: 50,
                    //     name: "Dnase-seq",
                    // },
                    {
                        type: "annotation",
                        format: "bed",
                        url: this.state.chromHMM_url_E104,
                        height: 50,
                        name: "ChromHMM_E104",
                        displayMode: "EXPANDED",
                    },
                    // {
                    //     type: "annotation",
                    //     format: "bed",
                    //     url: this.state.chromHMM_url_3,
                    //     height: 50,
                    //     name: "ChromHMM_E095",
                    //     displayMode: "EXPANDED",
                    // },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me3_url_lv,
                        height: 50,
                        name: "H3k4me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k36me3_url_lv,
                        height: 50,
                        name: "H3k36me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me1_url_lv,
                        height: 50,
                        name: "H3k4me1 Left Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k9me3_url_lv,
                        height: 50,
                        name: "H3k9me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27me3_url_lv,
                        height: 50,
                        name: "H3k27me3 Left Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27ac_url_lv,
                        height: 50,
                        name: "H3k27ac Left Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    
                   
                   

                    // {
                    //     type: "wig",
                    //     format: "bigwig",
                    //     url: this.state.CTCF_url,
                    //     height: 50,
                    //     name: "CTCF",
                    //     color: "rgb(252, 74, 3)",
                    // },

                   

                 
                    {
                        type: "annotation",
                        format: "bed",
                        url: this.state.chromHMM_url_E105,
                        height: 50,
                        name: "ChromHMM_E105",
                        displayMode: "EXPANDED",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me3_url_rv,
                        height: 50,
                        name: "H3k4me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k36me3_url_rv,
                        height: 50,
                        name: "H3k36me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k4me1_url_rv,
                        height: 50,
                        name: "H3k4me1 Right Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k9me3_url_rv,
                        height: 50,
                        name: "H3k9me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27me3_url_rv,
                        height: 50,
                        name: "H3k27me3 Right Ventricle",
                        color: "rgb(252, 74, 3)",
                    },
                    {
                        type: "wig",
                        format: "bigwig",
                        url: this.state.H3k27ac_url_rv,
                        height: 50,
                        name: "H3k27ac Right Ventricle",
                        color: "rgb(252, 202, 3)",
                    },
                    {
                        type: "annotation",
                        format: "bb",
                        url: process.env.REACT_APP_BASE_URL + "/igv/bigwig/encodeCcreCombined.bb",
                        height: 50,
                        name: "ENCODE-cCRE",
                        displayMode: "EXPANDED",
                    },
                    {
                        type: "annotation",
                        format: "gtf",
                        url: process.env.REACT_APP_BASE_URL + "/igv/gencode.v35.annotation.sort.gtf.gz",
                        indexURL: process.env.REACT_APP_BASE_URL + "/igv/gencode.v35.annotation.sort.gtf.gz.tbi",
                        //displayMode: "SQUISHED",
                        displayMode: "EXPANDED",
                        name: "Gencode v35 (gtf)",
                        visibilityWindow: 10000000
                    },
                ]
            };
            
            return igv.createBrowser(igvContainer, igvOptions);

        }
    }

    getGeneData =async () => {
        try{
            var gene_url = process.env.REACT_APP_EXPRESS_URL + "/gene/" + this.state.gene;
          console.log(gene_url);
          const res = await axios.get(gene_url);
          console.log(res.data);

          if(!res.data.length) {
            this.setState({
                geneInfo:false,
                geneLoading: true
            })
            console.log(this.state.snpResults)
          }else { 
            this.setState({
                geneData: res.data,
                geneInfo:true,
                geneLoading: true
            })
            this.setState({
                chr:this.state.geneData[0].chr.split('r')[1],
            })
           }
           console.log(this.state.geneData);
           console.log(this.state.geneInfo);
           console.log(this.state.chr);
         }catch (e) {
            console.log(e)
         }
    };


    getPromoterData =async () => {
        try{
            var gene_url = process.env.REACT_APP_EXPRESS_URL + "/promoter2gene/" + this.state.gene;
          console.log(gene_url);
          const res = await axios.get(gene_url);
          console.log(res.data);
          let tssAll=[];
          let tssUnique=[];
          if(res.data.length){
              for(let i=0; i<res.data.length; i++){
                  tssAll.push(res.data[i].TSS)
              }
              tssUnique=  tssAll.filter((item, i, ar) => ar.indexOf(item) === i);
          }
          console.log(tssAll);
          console.log(tssUnique);

        //   let snpRangeAll=[];
        //   let snpRangeAllUnique=[];
        //   if(tssUnique.length>0){
        //     for(let i=0; i<tssUnique.length; i++){
        //         for(let j=-10; j<10; j++){
        //             snpRangeAll.push(tssUnique[i]+j)
        //         }
        //     }
        //     snpRangeAllUnique = snpRangeAll .filter((item, i, ar) => ar.indexOf(item) === i);
        //     this.setState({
        //         final_allProximalList: snpRangeAllUnique,
        //     })
        //   }

        //   console.log(snpRangeAll);
        //   console.log(snpRangeAllUnique);





          this.setState({
              tssList: tssUnique,
          })
          console.log(this.state.tssList);

          if(!res.data.length) {
            this.setState({
                promoterResults:false,
                showPormoterTable: true
            })
          }else { 
            this.setState({
                promoterResults:true,
                promoterData: res.data,
            })
            console.log("have promoter data");
           }
           console.log(this.state.chr);
          this.getProximalRegionData();

         }catch (e) {
            console.log(e)
         }
    };


    // tssList: [],
    // proximalRegionInfo: false,
    // proximalRegionLoading:false,

    getProximalRegionData =async () => {
        try{
            console.log("this is getProxmialRegion Part");
            console.log(this.state.chr);
            if(this.state.tssList.length===0){
                this.setState({
                      
                   proximalResults: false,
                showProximalTable: true

                })
            }else{
                    console.log("start getProxmial serch!!!!!!!");
                    console.log(this.state.tssList);
                    let allProximalData=[];
                    for(let i=0; i<this.state.tssList.length; i++){
                        var proximal_url = process.env.REACT_APP_EXPRESS_URL + "/snp2range/"+this.state.chr+"_"+this.state.tssList[i];
                         console.log(proximal_url);
                        const res = await axios.get(proximal_url);
                         console.log(res.data);
                         for(let i=0; i<res.data.length; i++){
                            allProximalData.push(res.data[i])
                         }
                        
                    }
                    console.log(allProximalData);
                    console.log("END getProxmial serch!!!!!!!");
                    this.setState({
                        proximalResults:true,
                        proximalData: allProximalData,
                    })


                    // var proximal_url = process.env.REACT_APP_EXPRESS_URL + "/snp2range/"+this.state.chr+"_"+this.state.tssList[0];
                    // console.log(proximal_url);
                    // const res = await axios.get(proximal_url);
                    // console.log(res.data);
                    // this.setState({
                    //     proximalResults:true,
                    //     proximalData: res.data,
                    // })
                    
                   
                
              
            }
        }catch (e) {
            console.log(e)
         }
    };


    SNPDetail = (row, isSelect, rowIndex, e) => {
        //console.log(this.state.ldvariant);
        //console.log(this.state.cell);
        console.log(row);
        // const id = row.RSID;
        var variantID = row.variantID;
        // const variant_tmp = variantID.split(':');
        // const chr = variant_tmp[0]
        // const pos = variant_tmp[1]
        // const allele = variant_tmp[2].split('/')
        // const ref = allele[0]
        // const alt = allele[1]
        // variantID = chr + "_" + pos + "_" + ref + "_" + alt
        console.log(variantID)
        const snpURL = process.env.REACT_APP_BASE_URL + '/snps/' + this.state.cell + '/' + variantID;
        window.open(snpURL, "_blank")
    }

    generateIGVFile = async () => {
        try{
            var promoter_api_url;
            var columns_update;
            if (this.state.cell === "NHCFV") {
                promoter_api_url = process.env.REACT_APP_EXPRESS_URL + "/promoter2gene/" + this.state.gene;
                // columns_update = [
                //     { dataField: "RSID", text: "rsID"},
                //     { dataField: "ExonicFunc_Ensembl", text: "Function"},
                //     { dataField: "AAChange_Ensembl", text: "AAChange"},
                //     { dataField: "Promoter_like_region", text: "Promoter"},
                //     { dataField: "chromHMM_E026_25", text: "ChromHMM"},
                //     { dataField: "OpenChromatin_hMSC", text: "Open Chromatin"},
                //     { dataField: "ATAC-MBPS_hMSC-Day1", text: "TF footprinting"},
                //     { dataField: "SigHiC_hMSC", text: "Hi-C info"},
                // ]
                this.setState({
                    atac_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/NHCFVcombined_R1.trim.merged.nodup.tn5.pval.signal.bigwig',
                    chromHMM_url_E104: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/E104_25_imputed12marks_hg38lift_dense.bed',
            chromHMM_url_E105: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/E095_25_imputed12marks_hg38lift_dense.bed',

            H3k4me3_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF285MYB.bigWig',
            H3k36me3_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF815KRX.bigWig',
            H3k4me1_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF213UYA.bigWig',
            H3k9me3_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF294DMS.bigWig',
            H3k27me3_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF289STQ.bigWig',
            H3k27ac_url_lv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF094PUR.bigWig',
            H3k4me3_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF026UGX.bigWig',
            H3k36me3_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF443LCS.bigWig',
            H3k4me1_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF240MYO.bigWig',
            H3k9me3_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF136HCH.bigWig',
            H3k27me3_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF217IRO.bigWig',
            H3k27ac_url_rv: process.env.REACT_APP_BASE_URL + '/igv/bigwig/NHCFV/ENCFF446QTP.bigWig',
                    // exonicColumns: columns_update,
                    // atac_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/ATAC_seq-hMSC_pvalue.bigwig',
                    // dnase_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/DNase_seq_hMSC.bigWig',
                    // chromHMM_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/E026_25_imputed12marks_hg38lift_dense.bed',
                    // H3k27ac_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/ENCFF223NOV_hMSC_H3K27Ac_pvalue.bigWig',
                    // H3k4me3_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/ENCFF611FKW_hMSC_H3K4me3_pvalue.bigWig',
                    // H3k4me1_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/ENCFF648BRC_hMSC_H3K4me1_pvalue.bigWig',
                    // H3k9ac_url: process.env.REACT_APP_BASE_URL + '/igv/bigwig/hMSC/ENCFF757SDY_hMSC_H3K9ac_pvalue.bigWig',
                })
            }
            console.log(promoter_api_url)
            const res_promoter = await axios.get(promoter_api_url);
            var promoter_update = res_promoter.data
            var igv_bedpe_content;
            var start_min = 1000000000000000;
            var end_max = 0;
            for(var i=0;i<promoter_update.length;i++){
                var hicPromoterBin = promoter_update[i].HiC_Promoter_bin.split(':')
                var hicPromoterBinChr = hicPromoterBin[0];
                var hicPromoterBinStart = hicPromoterBin[1];
                var hicPromoterBinEnd = hicPromoterBin[2];
                var hicRegBin = promoter_update[i].HiC_Distal_bin.split(':')
                var hicRegBinChr = hicRegBin[0];
                var hicRegBinStart = hicRegBin[1];
                var hicRegBinEnd = hicRegBin[2];
                if(hicPromoterBinStart < start_min){
                    start_min = hicPromoterBinStart
                }
                if(hicRegBinStart < start_min){
                    start_min = hicRegBinStart
                }
                if(hicPromoterBinEnd > end_max){
                    end_max = hicPromoterBinEnd
                }
                if(hicRegBinEnd > end_max){
                    end_max = hicRegBinEnd
                }
                var each_interaction = "chr" + hicRegBinChr + "\t" + hicRegBinStart + "\t" + hicRegBinEnd + "\tchr" + hicPromoterBinChr + "\t" + hicPromoterBinStart + "\t" + hicPromoterBinEnd + "\t10\n";
                if(igv_bedpe_content == null){
                    igv_bedpe_content = each_interaction
                }else{
                    igv_bedpe_content += each_interaction
                }
                
                //console.log('each_interaction:' + each_interaction)
                promoter_update[i].Chr = promoter_update[i].Chr + ":" + promoter_update[i].Start + "-" + promoter_update[i].End
                //console.log('promoter:' + promoter_update[i].Chr)
            }
            const response = await fetch(process.env.REACT_APP_EXPRESS_URL + '/writeFile',{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    type: "interactionByGene",
                    cell: this.state.cell,
                    gene: this.state.gene,
                    content: igv_bedpe_content,
                }),
            });
            const writeRes = await response.text();
            console.log("write file:")
            console.log(writeRes)
            var start_pos = parseInt(start_min) - 10000
            var end_pos = parseInt(end_max) + 10000
            if (start_pos < 0){
                start_pos = 0
            }
            console.log("locus range:chr" + hicPromoterBinChr + ":" + start_pos + "-" + end_pos)
            this.setState({
                locus_range: "chr" + hicPromoterBinChr + ":" + start_pos + "-" + end_pos,
                locus_hic_url: process.env.REACT_APP_BASE_URL + "/igv/" + this.state.gene + "_" + this.state.cell + ".bedpe.txt",
                isLoading: false,
                showTable: true,
                snpResults: true,
                showIGV: true,
            })
            return "generate IGV data successfully";

        } catch (e) {
            console.log(e)
        }
    }
    
    getSNPData = async () => {
        try{
          //alert(trait_name)
          //console.log(this.state.trait_name);
          var snp_url = process.env.REACT_APP_EXPRESS_URL + "/snp2gene/" + this.state.gene;
          console.log(snp_url);
          const res = await axios.get(snp_url);
          console.log(res.data);
          //console.log("type of data:" + typeof res.data)
          if(!res.data.length) {
            this.setState({
                snpResults: false,
                showTable: true,
            })
            console.log(this.state.snpResults)
          }else { 
            this.setState({
                snps: res.data,
                snpResults:true,
                isLoading:false,
            })
            console.log(this.state.snpResults)
            const res_generateIGV = await this.generateIGVFile();
            console.log("res_generateIGV:")
            console.log(res_generateIGV);
            
          }
          
           
        } catch (e) {
            console.log(e)
        }
    };
    handleChangeGene = (event) => {
        this.setState({ gene : event.target.value });
    }
    handleChangeCell = (event) => {
        this.setState({ cell : event.target.value });
    }
    
    handleSubmit = (event) => {
        console.log(this.state.gene);
        console.log(this.state.cell);
        this.setState({
            showTable: false,
            showPormoterTable: false,
            // snpResults: false,
            showIGV: false,
            isLoading: true,
           
        })
       
        this.getSNPData();
        //alert('A name was submitted: ' + this.state.rsid);
        console.log(this.state.chr);
        this.getGeneData();
        console.log("before getpromoterdata");
        console.log(this.state.chr);
        this.getPromoterData();
        console.log("after get promoterdata");
        event.preventDefault();
        
    }
    
    render() {
        //const igv_ele = this.getIGV();
      
        return (
            <div className="content">
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                            <CardTitle tag="h4">Gene</CardTitle>
                            </CardHeader>
                            
                            <CardBody>
                                <Form onSubmit={this.handleSubmit}>
                                    <FormGroup className="no-border"> 
                                        <Input 
                                            type="text" 
                                            value={this.state.gene} 
                                            placeholder="Search Gene..." 
                                            onChange={this.handleChangeGene}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Select Cell Type</Label>
                                        <Input type="select">
                                            <option>NHCFV</option>
                                            <option>NHCFA</option>
                                            <option>Cardiomyocytes</option>
                                        </Input>
                                        </FormGroup>
                                    
                                    <Button>Submit</Button>
                                    
                                    
                                </Form>
                            </CardBody>
                        </Card>
                    </Col>

                    { (this.state.geneInfo)
                        ? <Col md="12">
                            <Card className="content">
                            <CardHeader>
                                    <CardTitle tag="h4">{this.state.geneData[0].gene_symbol}</CardTitle>
                                    </CardHeader>
                                <CardBody>
                                    {/* <h5>
                                    Gene Symbol: {this.state.geneData[0].gene_symbol} 
                                    </h5> */}
                                    <h5>
                                    Full Name: {this.state.geneData[0].full_name} 
                                    </h5>
                                    <h5>
                                    Chr: {this.state.geneData[0].chr} 
                                    </h5>
                                    
                                    <h5>
                                    Start: {this.state.geneData[0].start} 
                                    </h5>
                                    <h5>
                                    End: {this.state.geneData[0].end} 
                                    </h5>
                                </CardBody>
                                
                            </Card>
                        </Col>
                        : (this.state.geneInfo === false && this.state.geneLoading === true) ? 
                        <Col md="12">
                            <Card>
                                <CardHeader>
                                <CardTitle tag="h4">Can't find this {this.state.gene} </CardTitle>
                                </CardHeader>
                            </Card>
                        </Col>
                        : null
                    }

                    {(this.state.snpResults && this.state.showIGV)
                        ? <Col md="12">
                            <Card>
                                <CardHeader>
                                <CardTitle tag="h4">{this.state.gene}'s IGV in {this.state.cell} cell-type</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <div id="igv-div" style={igvStyle}></div>
                                </CardBody>
                            </Card>
                        </Col>
                        : null
                    }
                    <Nav tabs>
                    
                        <NavItem>
                            <NavLink
                            href="#"
                            className={classnames({ active: this.state.activeTab === '1' })}
                            onClick={() => {
                                this.toggle('1');
                            }}
                            >
                            Coding Region
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                            href="#"
                            className={classnames({ active: this.state.activeTab === '2' })}
                            onClick={() => {
                                this.toggle('2');
                            }}
                            >
                            Proximal regulatory region
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                            href="#"
                            className={classnames({ active: this.state.activeTab === '3' })}
                            onClick={() => {
                                this.toggle('3');
                            }}
                            >
                            Distal regulatory region
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <Row>
                        { (this.state.snpResults && !this.state.isLoading)
                        ? 
                            <Col md="12">
                                <Card>
                                    <CardHeader>
                                    <CardTitle tag="h4">Coding Region of {this.state.gene} in {this.state.cell} cell-type</CardTitle>
                                    </CardHeader>
                                    <CardBody>
                                    <div className="SNPTable">
                                        {/* <ToolkitProvider
                                            keyField="_id"
                                            data={this.state.snps}
                                            columns={this.state.exonicColumns}
                                            >
                                            {
                                                props => (
                                                <div>
                                                   <hr />
                                                    <BootstrapTable
                                                        pagination={paginationFactory(this.options)}
                                                        striped
                                                        hover
                                                        condensed
                                                        { ...props.baseProps }
                                                    />
                                                </div>
                                                )
                                            }
                                        </ToolkitProvider> */}
                                         <BootstrapTable
                                            keyField="_id"
                                            //data={this.state.snps}
                                            data={this.state.snps}
                                            columns={this.exonicColumns}
                                            striped
                                            hover
                                            condensed
                                            pagination={paginationFactory()}
                                        />     
                                      
                                    </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        : (this.state.snpResults === false && this.state.showTable === true) ? 
                            <Col md="12">
                                <Card>
                                    <CardHeader>
                                    <CardTitle tag="h4">Can't find any {this.state.gene} relevant SNPs</CardTitle>
                                    </CardHeader>
                                </Card>
                            </Col>
                        : (this.state.isLoading) ? 
                            <Col md="12">
                                <Loader type="spin" />
                            </Col>
                        : null 
                        }
                        </Row>
                    </TabPane>
                   
                    <TabPane tabId="2">
                        <Row>
                       
                        { (this.state.proximalResults && !this.state.proximalRegionLoading)
                        ? 
                            <Col md="12">
                                <Card>
                                    <CardHeader>
                                    <CardTitle tag="h4">Proximal Region of {this.state.gene} in {this.state.cell} cell-type</CardTitle>
                                    </CardHeader>
                                    <CardBody>
                                    <div className="ProximalTable">
                                       
                                         <BootstrapTable
                                            keyField="_id"
                                            data={this.state.proximalData}
                                            columns={this.proximalRegionColumns}
                                            selectRow={this.selectSNP}
                                            striped
                                            hover
                                            condensed
                                            pagination={paginationFactory()}
                                        />     
                                      
                                    </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        : (this.state.proximalResults === false && this.state.showPormoterTable === true) ? 
                            <Col md="12">
                                <Card>
                                    <CardHeader>
                                    <CardTitle tag="h4">Can't find any {this.state.gene} relevant SNP in Proximal Region</CardTitle>
                                    </CardHeader>
                                </Card>
                            </Col>
                        : (this.state.proximalRegionLoading) ? 
                            <Col md="12">
                                <Loader type="spin" />
                            </Col>
                        : null 
                        }
                        </Row>
                    </TabPane>
                    <TabPane tabId="3">
                        <Row>
                        
                        </Row>
                    </TabPane>
                    </TabContent>
                       
                </Row>
            </div>
        );
    }
}

export default Gene1;