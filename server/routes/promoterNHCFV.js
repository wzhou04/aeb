const router = require('express').Router();
const fs = require('fs')
// let Promoter = require('../models/promoter.model');
let Promoter_NHCFV = require('../models/promoter.NHCFV.model');


router.route('/:regulatoryBin').get(async(req, res) => {
    try{
        await Promoter_NHCFV.find( {"HiC_Distal_bin": req.params.regulatoryBin} )
        .then(regulatoryBin => res.json(regulatoryBin))       
    } catch (error) {
        console.log(error)
    }
});

module.exports = router;