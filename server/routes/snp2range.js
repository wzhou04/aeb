const router = require('express').Router();
// let dbSNP153_Annotation = require('../models/snp.model');
let dbSNP = require('../models/snp.model');


router.route('/:chr_position').get(async(req, res) => {
    try{
        let chr_position=req.params.chr_position;
        let chr=chr_position.split("_")[0];
        let position=chr_position.split("_")[1];
        await dbSNP.find({"$and": [{"#Chr": parseInt(chr)},{ "Start" : {$gt : parseInt(position, 10)-1000, $lt: parseInt(position, 10)+1000} } ] })
        // await dbSNP.find({"$and": [{"#Chr": parseInt(chr)},{ "Start" : parseInt(position) } ] })
            .then(snp => res.json(snp))
    }catch (error) {
        console.log(error);
    }
})

module.exports = router;