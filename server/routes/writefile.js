const router = require('express').Router();
const fs = require('fs');

const igvDir = __dirname + '/../../public/igv/'
var igvInput;
router.route('/').post((req, res) => {
    if(req.body.type == "snp"){
        const rsid = req.body.post;
        const content = req.body.range;
        igvInput = igvDir + rsid + '.bed'
        if(fs.existsSync(igvDir)){
           
            fs.writeFile(igvInput, content, err => {
                    if(err) {
                        console.log(err)
                        return err
                    }
                })
        }else{
            console.log('Directory not found.')
            return 'Directory not found.';
        }
        
    }else if(req.body.type == "interaction"){
        const rsid = req.body.rsid;
        const content = req.body.content;
        const cell = req.body.cell;
        igvInput = igvDir + rsid + '_' + cell + '.bedpe.txt';
        
        fs.writeFile(igvInput, content, err => {
            if(err) {
                console.log(err)
                return
            }
        })
    }else if(req.body.type == "interactionByGene"){
        const gene = req.body.gene;
        const content = req.body.content;
        const cell = req.body.cell;
        igvInput = igvDir + gene + '_' + cell + '.bedpe.txt';
        
        fs.writeFile(igvInput, content, err => {
            if(err) {
                console.log(err)
                return
            }
        })
    }
    res.send(
        `Successfully generate file: ${igvInput}`,
    )
  
    
});

module.exports = router;