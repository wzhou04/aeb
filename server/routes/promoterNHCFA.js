const router = require('express').Router();
const fs = require('fs')
let Promoter_NHCFA = require('../models/promoter.NHCFA.model');



router.route('/:regulatoryBin').get(async(req, res) => {
    try{
        await Promoter_NHCFA.find( {"HiC_Distal_bin": req.params.regulatoryBin} )
        .then(regulatoryBin => res.json(regulatoryBin))       
    } catch (error) {
        console.log(error)
    }
});

module.exports = router;