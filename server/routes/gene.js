const router = require('express').Router();
let gene = require('../models/gene.model');

router.route('/:gene').get(async(req, res) => {
    try{
        await gene.find( {"gene_symbol": req.params.gene} )
        .then(gene => res.json(gene))       
    } catch (error) {
        console.log(error)
    }
});

module.exports = router;