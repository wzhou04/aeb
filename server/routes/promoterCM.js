const router = require('express').Router();
const fs = require('fs')
// let Promoter = require('../models/promoter.model');
let Promoter_CM = require('../models/promoter.CM.model');


router.route('/:regulatoryBin').get(async(req, res) => {
    try{
        await Promoter_CM.find( {"HiC_Distal_bin": req.params.regulatoryBin} )
        .then(regulatoryBin => res.json(regulatoryBin))       
    } catch (error) {
        console.log(error)
    }
});

module.exports = router;