const router = require('express').Router();
let Promoter_CM = require('../models/promoter.CM.model');

router.route('/:gene').get(async(req, res) => {
    try{
        await Promoter_CM.find( {"Gene": req.params.gene} )
        .then(gene => res.json(gene))       
    } catch (error) {
        console.log(error)
    }
});

module.exports = router;