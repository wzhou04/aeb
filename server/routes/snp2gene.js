const router = require('express').Router();
// let dbSNP153_Annotation = require('../models/snp.model');
let dbSNP = require('../models/snp.model');


router.route('/:gene').get(async(req, res) => {
    try{
        // await dbSNP153_Annotation.find({ $and: [ {"GeneName_ID_Ensembl": req.params.gene}, {"Region": "exonic"}] })
        await dbSNP.find({ $and: [ {"GeneName_ID_Ensembl": req.params.gene}, {"Region_Ensembl": "exonic"}] })
            .then(gene => res.json(gene))
    }catch (error) {
        console.log(error);
    }
})

module.exports = router;