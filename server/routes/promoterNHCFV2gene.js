const router = require('express').Router();
let Promoter_NHCFV = require('../models/promoter.NHCFV.model');

router.route('/:gene').get(async(req, res) => {
    try{
        await Promoter_NHCFV.find( {"Gene": req.params.gene} )
        .then(gene => res.json(gene))       
    } catch (error) {
        console.log(error)
    }
});

module.exports = router;