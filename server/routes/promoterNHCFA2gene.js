const router = require('express').Router();
let Promoter_NHCFA = require('../models/promoter.NHCFA.model');

router.route('/:gene').get(async(req, res) => {
    try{
        await Promoter_NHCFA.find( {"Gene": req.params.gene} )
        .then(gene => res.json(gene))       
    } catch (error) {
        console.log(error)
    }
});

module.exports = router;