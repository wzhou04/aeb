const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AnnotationSchema = new Schema({
}, {
    // collection: 'dbSNP153_Annotation'
    collection: 'dbSNP'

});

// const dbSNP153_Annotation = mongoose.model('dbSNP153_Annotation', AnnotationSchema);
const dbSNP = mongoose.model('dbSNP', AnnotationSchema);

// module.exports = dbSNP153_Annotation;
module.exports = dbSNP;