const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const promoter_NHCFVSchema = new Schema({
}, {
    collection: 'Promoter'
});

const Promoter_NHCFV = mongoose.model('Promoter_NHCFV', promoter_NHCFVSchema);

module.exports = Promoter_NHCFV;