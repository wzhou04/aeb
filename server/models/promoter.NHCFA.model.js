const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const promoter_NHCFASchema = new Schema({
}, {
    collection: 'Promoter_NHCFA'
});

const Promoter_NHCFA = mongoose.model('Promoter_NHCFA', promoter_NHCFASchema);

module.exports = Promoter_NHCFA;