const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 8080;

app.use(cors());
app.use(express.json());

//connect to mongoDB
const db_ip = process.env.MONGO_IP;
const db_port = process.env.MONGO_PORT;
const db_name = process.env.MONGO_DB;
// const db_user = process.env.MONGO_USER;
// const db_password = process.env.MONGO_PWD;
const MONGO_URI = `${db_ip}:${db_port}`;
// const db_URI = `mongodb://${db_user}:${db_password}@${MONGO_URI}/${db_name}`;
const db_URI = `mongodb://${MONGO_URI}/${db_name}`;
console.log(`db_URI: ${db_URI}`);
//mongoose.connect(db_URI, { useNewUrlParser: true, useUnifiedTopology: true});
mongoose
  .connect(db_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }) // Adding new mongo url parser
  .then(() => console.log('MongoDB Connected successfully'))
  .catch(err => console.log(err));




//routes
const geneRouter=require('./routes/gene');
const snpRouter = require('./routes/snp');
const snp2geneRouter = require('./routes/snp2gene');
const diseaseRouter = require('./routes/disease');
const promoterRouter=require('./routes/promoter');
const promoter2geneRouter = require('./routes/promoter2gene');
const promoterNHCFVRouter = require('./routes/promoterNHCFV');
const promoterNHCFV2geneRouter = require('./routes/promoterNHCFV2gene');
const snp2rangeRouter=require('./routes/snp2range');
const gwas2geneRouter = require('./routes/gwasSNP');
const gwasLD = require('./routes/gwasLD');
const variantID = require('./routes/variantID');
// const promoterNHCFARouter = require('./routes/promoterNHCFA');
// const promoterNHCFA2geneRouter = require('./routes/promoterNHCFA2gene');
// const promoterCMRouter = require('./routes/promoterCM');
// const promoterCM2geneRouter = require('./routes/promoterCM2gene');
const writeLocalFile = require('./routes/writefile');



app.use('/gene', geneRouter);
app.use('/snp', snpRouter);
app.use('/snp2gene', snp2geneRouter);
app.use('/snp2range', snp2rangeRouter);
 app.use('/disease', diseaseRouter);
app.use('/promoter', promoterRouter);
app.use('/promoter2gene', promoter2geneRouter);
app.use('/gwas', gwas2geneRouter);
app.use('/gwasLD', gwasLD);
app.use('/promoterNHCFV',promoterNHCFVRouter);
app.use('/promoterNHCFV2gene', promoterNHCFV2geneRouter);
// app.use('/promoterNHCFA',promoterNHCFARouter);
// app.use('/promoterNHCFA2gene', promoterNHCFA2geneRouter);
// app.use('/promoterCM',promoterCMRouter);
// app.use('/promoterCM2gene', promoterCM2geneRouter);
app.use('/writeFile', writeLocalFile);
app.use('/variantID', variantID);
app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});